public class Main {

    public static void main(String[] args) {
        System.out.println(nomModul());
        System.out.println(grup());
        System.out.println(convocatoria());
    }

    public static String nomModul(){
        return "M05";
    }

    public static String grup(){
        return "B";
    }

    public static String convocatoria(){
        return "Convocatòria extraordinària";
    }

}
